require('dotenv').config();
const Discord = require('discord.js')
const client = new Discord.Client()
const TOKEN = process.env.TOKEN

const TheCount = require('./bot/TheCount')

client.login(TOKEN);

client.on('ready', async () => {
  console.info(`Logged in as ${client.user.tag}!`)
  TheCount.handleReady(client)
})


client.on('message', msg => {
  TheCount.handleMessage(msg)
})

class Responses {
  whatsUp = [
    ` counting how many apples I have`,
    ` counting how many friends I have`,
    ` counting how many grapes I have`,
    ` counting how many pens I have`,
    ` counting all the grains of rice in my pantry`,
    ` counting all the lines on my notebook`,
    ` counting all the pairs of shoes I own`,
    ` counting all the hairs on my head`,
    ` counting all the tiles in my bathroom`,
    ` counting how many letters there are in all the alphabets in the world`,
    ` just counting numbers because why not`,
    ` counting all the bats in my manor`,
    ` counting all the candles on the shelf`,
    ` counting all the spiders on the wall`,
    ` counting all the cobbwebs in the hall`,
    ` i am alone so I am counting myself`,
    ` counting how many lobby wobbies I need until 99 fishing`
  ]
}

module.exports = new Responses()
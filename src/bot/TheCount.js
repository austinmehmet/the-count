const Thread = require('../services/Thread')
const RandomNumberGenerator = require('../services/RandomNumberGenerator')
const Responses = require('./Responses')

class TheCount {
  async handleReady(client) {
    let countingChannelKey = null
    client.channels.cache.forEach((value, key, map) => {
      if (value.name === 'counting') {
        countingChannelKey = key
      }
    })
    const channel = await client.channels.fetch(countingChannelKey)
    const messages = await channel.messages.fetch({ limit: 1 })
    let lastMessage = messages.first()
    let num = lastMessage.content
    num++
    while (true) {
      channel.send(num)
      await Thread.sleep(1500)
      num++
    }
  }

  handleMessage(msg) {
    if (msg.content.toLowerCase() === 'whats up count') {
      const response = Responses.whatsUp[Math.floor(Math.random() * Responses.whatsUp.length)]
      const randomNumber = RandomNumberGenerator.generate()
      msg.reply(`${response}. I'm at ${randomNumber}`);
    } else if (msg.content.toLowerCase() === 'vampires suck') {
      msg.reply(' well that is rude')
    } else if (msg.content.toLowerCase() === 'count, you know of any good music?') {
      msg.reply('https://www.youtube.com/watch?v=ZIniljT5lJI')
    }
  }
  
}

module.exports = new TheCount()